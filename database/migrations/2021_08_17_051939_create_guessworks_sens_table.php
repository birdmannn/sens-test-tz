<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuessworksSensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guessworks_sens', function (Blueprint $table) {
            $table->id();
            $table->integer('number');
            $table->integer('rait')->default(0);
            $table->unsignedBigInteger('guesswork_id', false);
            $table->foreign('guesswork_id')->references('id')->on('guessworks')->cascadeOnUpdate()->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guessworks_sens');
    }
}
