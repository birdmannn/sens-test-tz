<?php

use App\Http\Controllers\CalculateController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('index');
})->name('index');

Route::post('/sent', [CalculateController::class, 'sent'])->name('user-number');
Route::post('/check', [CalculateController::class, 'check'])->name('check');
Route::get('/index-meow', function () {
    return redirect(route('index'));
})->name('index-meow');
