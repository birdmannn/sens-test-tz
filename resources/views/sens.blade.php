@extends('layouts.main')
@section('content')
    <section class="d-flex align-items-center">
        <div class="container d-flex flex-column align-items-center justify-content-center" data-aos="fade-up">
            <form method="POST" action="{{ route('check') }}">
                @csrf
                <label>
                    Теперь введите число что вы зададывали
                    <input type="number" name="number">
                </label>
            </form>
        </div>
    </section>

    <section class="services">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Ниже самые близкие догадки наших экстрасенсов</h2>
            </div>
                <div class="row">
                    @foreach($senses as $sens)
                    <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="fade-up" data-aos-delay="100">
                        <div class="icon-box">
                            <div class="icon"><i class="bx bxl-dribbble"></i></div>
                            <h4 class="title">{{ $sens }}</h4>
                            <p class="description"></p>
                        </div>
                    </div>
                    @endforeach
                </div>
        </div>
    </section>
@endsection
