@extends('layouts.main')
@section('content')
    <section class="d-flex align-items-center">
        <div class="container d-flex flex-column align-items-center justify-content-center" data-aos="fade-up">
            @if($number != $closest)
                <h1>Вы загадали {{ $number }}</h1>
                <h1>Ближайшее названное экстрасенсом число {{ $closest }}</h1>
            @else
                <h1>Экстрасенc угадал!</h1>
            @endif
            <form method="GET" action="{{ route('index-meow') }}">
                <button type="submit">Заново</button>
            </form>
            <p>Вы загадывали числа</p>
            @foreach($numbers as $numb)
                <p>Число - {{ $numb }}</p>
            @endforeach
            <p>Топ экстрасенсов</p>
            @foreach($sum_rait as $key => $sum)
                <p>Экстрасенс номер {{ $key + 1 }} счёт {{ $sum }}</p>
            @endforeach
            <img src="{{ asset('assets/img/hero-img.png') }}" class="img-fluid hero-img" >
        </div>
    </section>
@endsection
