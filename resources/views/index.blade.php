@extends('layouts.main')
@section('content')
    <section class="d-flex align-items-center">

        <div class="container d-flex flex-column align-items-center justify-content-center" data-aos="fade-up">
            <h1>Загадай двухзнчное чисто</h1>
            <h2>А наши специально обученные экстрасены отгадают его!</h2>
            <form method="POST" action="{{ route('user-number') }}">
                @csrf
                <button type="submit">Загадал</button>
            </form>
            <img src="{{ asset('assets/img/hero-img.png') }}" class="img-fluid hero-img" >
        </div>

    </section>
@endsection
