<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guesswork extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ['number'];

    public function numbers(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(GuessworksSens::class, 'guesswork_id');
    }
}
