<?php

namespace App\Http\Controllers;

use App\Models\Guesswork;
use App\Models\GuessworksSens;
use Illuminate\Http\Request;

class CalculateController extends Controller
{
    public function sent(Request $request) {
        $session = $request->session();
        $senses = [];
        for ($i = 1; $i <= 4; $i++) {
            $senses[] = rand(10, 99);
        }
        $session->put('senses', $senses);
        $session->put('senses', []);
        return view('sens', ['senses' => $senses]);
    }

    public function check(Request $request) {
        $request->validate(['number' => 'integer|min:10|max:99']);
        $number = $request->get('number');
        $s = $request->session();
        $numbers = $s->get('numbers');
        $numbers[] = $number;
        $s->put('numbers', $numbers);
        $senses = $s->get('senses');
        $s->forget('senses');
        $closest = $this->getClosest($number, $senses);
        $g = new Guesswork(['number' => $number]);
        $all = collect($senses)->map(function ($sens) use ($number, $senses, $closest) {
            return ['number' => $sens, 'rait' => $closest == $sens ? in_array($number, $senses) ? 2 : 1 : 0];
        })->sortBy(function ($obj) {
            return $obj['rait'];
        });
        $g->save();
        $g->numbers()->createMany($all);
        $sum = [0 => 0, 1 => 0, 2 => 0, 3 => 0];
        GuessworksSens::all()->chunk(4)->each(function ($chunk) use (&$sum) {
            $i = 0;
            foreach ($chunk as $item) {
                $sum[$i] += $item->rait;
                $i++;
            }
        });
        return view('index-new', ['closest' => $closest, 'number' => $number, 'sum_rait' => $sum, 'numbers' => $s->get('numbers')]);
    }

    private function getClosest($search, $arr) {
        $closest = null;
        foreach ($arr as $item) {
            if ($closest === null || abs($search - $closest) > abs($item - $search)) {
                $closest = $item;
            }
        }
        return $closest;
    }
}
